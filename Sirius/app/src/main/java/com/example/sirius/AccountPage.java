package com.example.sirius;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AccountPage extends AppCompatActivity {

    private ListView list;
    private List<String> recipeList = new ArrayList<>();
    private List<Integer> imageList = new ArrayList<>();
    private List<String> dateList = new ArrayList<>();
    private String username;
    private Button searchButton;
    private TextView searchText;
    private TextView user;
    private Button insertButton;
    private Button detailButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_layout);
        setBindings();
        setListeners();
        setRecipeList();
    }

    private void setBindings() {
        Bundle b = getIntent().getExtras();
        username = ""; // or other values
        if (b != null)
            username = b.getString("username");
        searchButton = (Button) findViewById(R.id.account_search_button);
        searchText = (TextView) findViewById(R.id.account_search_text);
        insertButton = (Button) findViewById(R.id.insert_button);
        detailButton = (Button) findViewById(R.id.account_to_detail);
    }

    private void setListeners() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlBase = "http://" + getString(R.string.ip_address) + "/food/listFoodsOf/?";
                StringBuilder string = new StringBuilder();
                string.append(urlBase);
                string.append("username=");
                string.append(username);
                string.append("%-%" + searchText.getText().toString());
                new Background().execute(string.toString());
            }
        });
        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountPage.this, CreateRecipe.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", username); //Your id
                intent.putExtras(bundle); //Put your id to your next Intent
                startActivity(intent);
            }
        });
        detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountPage.this, AccountDetails.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", username); //Your id
                intent.putExtras(bundle); //Put your id to your next Intent
                startActivity(intent);
            }
        });
    }

    private void setRecipeList() {
        String urlBase = "http://" + getString(R.string.ip_address) + "/food/listFoodsOf/?";
        StringBuilder string = new StringBuilder();
        string.append(urlBase);
        string.append("username=");
        string.append(username);
        string.append("%-%");
        new Background().execute(string.toString());
        // Array of strings for ListView Title
    }

    class Background extends AsyncTask<String, String, String> {

        protected String doInBackground (String ...params){
            HttpURLConnection connection = null;
            BufferedReader br = null;
            String link = params[0].split("%-%")[0];
            String search = "";
            if (params[0].split("%-%").length > 1)
                search = params[0].split("%-%")[1];
            try {
                URL url = new URL(link);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String satir;
                String dosya = "";
                while ((satir = br.readLine()) != null) {
                    dosya += satir;
                }
                return dosya + "%-%" + search;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hata var";
        }
        protected void onPostExecute(String s){
            recipeList = new ArrayList<>();
            String foodName;
            String createDate;
            String search = "";
            if (s.split("%-%").length > 1)
                search = s.split("%-%")[1];
            s = s.split("%-%")[0];
            try {
                final JSONArray obj = new JSONArray(s);
                final int n = obj.length();
                for (int i = 0; i < n; ++i) {
                    final JSONObject user = obj.getJSONObject(i);
                    foodName = user.getString("foodName");
                    createDate = user.getString("createDate");
                    String ingredients = user.getString("ingredients");
                    String tag = user.getString("tag");
                    String lastComment = user.getString("lastComment");
                    String foodSteps = user.getString("foodSteps");
                    search = search.toLowerCase();
                    if (search.equals("") || foodName.toLowerCase().indexOf(search)>= 0 || ingredients.toLowerCase().indexOf(search)>= 0 || tag.toLowerCase().indexOf(search)>= 0
                    || lastComment.toLowerCase().indexOf(search) >= 0 || foodSteps.toLowerCase().indexOf(search) >=0 ) {
                        recipeList.add(foodName);
                        dateList.add(createDate);
                        imageList.add(R.drawable.plate);
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

            for (int i = 0; i < recipeList.size(); i++) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("food_name", recipeList.get(i));
                hm.put("food_date", dateList.get(i));
                hm.put("food_image", Integer.toString(imageList.get(i)));
                aList.add(hm);
            }
            String[] from = {"food_image", "food_name", "food_date"};
            int[] to = {R.id.listview_image, R.id.listview_item_title, R.id.listview_item_date};

            SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.list_view_activity, from, to);
            list = (ListView) findViewById(R.id.account_recipe_list);
            list.setAdapter(simpleAdapter);
            user = (TextView) findViewById(R.id.account_username);
            user.setText(username);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                {
                    LinearLayout x = (LinearLayout) arg1;
                    String recipe_name = ((TextView)x.findViewById(R.id.listview_item_title)).getText().toString();
                    String recipe_date = ((TextView)x.findViewById(R.id.listview_item_date)).getText().toString();
                    Intent intent = new Intent(AccountPage.this, RecipeInfo.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username); //Your id
                    bundle.putString("foodName",recipe_name);
                    bundle.putString("createDate", recipe_date);
                    intent.putExtras(bundle); //Put your id to your next Intent
                    startActivity(intent);
                }
            });
        }
    }
}
