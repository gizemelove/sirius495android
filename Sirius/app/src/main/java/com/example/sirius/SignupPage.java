package com.example.sirius;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupPage extends AppCompatActivity {

    public String username;
    private Button signupButton;
    private EditText nameText, surnameText, usernameText, emailText, passwordText , confirmPasswordText;
    private TextView alreadyUserLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        bindViews();
        setListeners();
    }

    private void bindViews() {
        nameText = (EditText) findViewById(R.id.signup_name_input);
        surnameText = (EditText) findViewById(R.id.signup_surname_input);
        usernameText = (EditText) findViewById(R.id.signup_username_input);
        emailText = (EditText) findViewById(R.id.signup_email_input);
        passwordText = (EditText) findViewById(R.id.signup_password_input);
        confirmPasswordText = (EditText) findViewById(R.id.signup_password_input2);
        alreadyUserLink = (TextView) findViewById(R.id.already_user_link);
        signupButton = (Button) findViewById(R.id.signup_button);
    }

    private void setListeners() {
        alreadyUserLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupPage.this, LoginPage.class);
                startActivity(intent);
            }
        });
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameText.getText().toString();
                String surname = surnameText.getText().toString();
                username = usernameText.getText().toString();
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                String confirmPassword = confirmPasswordText.getText().toString();
                if (!password.equals(confirmPassword)) {
                    Toast.makeText(getApplicationContext(), "Passwords didn't match. Please try again.", Toast.LENGTH_LONG).show();
                } else {
                    String url = "http://" + getString(R.string.ip_address) +  "/user/addUser";
                    // Optional Parameters to pass as POST request
                    JSONObject js = new JSONObject();
                    try {
                        js.put("name",name);
                        js.put("surname",surname);
                        js.put("mail",email);
                        js.put("username",username);
                        js.put("password",password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // Make request for JSONObject
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                            Request.Method.POST, url, js,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("**", response.toString());
                                    FirebaseMessaging.getInstance().subscribeToTopic(username);
                                    Intent intent = new Intent(SignupPage.this, AccountPage.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", username); //Your id
                                    intent.putExtras(bundle); //Put your id to your next Intent
                                    startActivity(intent);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("**", error.toString());
                            Toast.makeText(getApplicationContext(), "Connection problem. Please check your connection", Toast.LENGTH_LONG).show();
                        }
                    }) {

                        /**
                         * Passing some request headers
                         */
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            return headers;
                        }

                    };
                    // Adding request to request queue
                    Volley.newRequestQueue(SignupPage.this).add(jsonObjReq);

                }
            }
        });
    }
}