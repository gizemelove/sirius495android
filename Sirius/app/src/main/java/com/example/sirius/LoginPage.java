package com.example.sirius;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.messaging.FirebaseMessaging;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginPage extends AppCompatActivity {
    private String username;
    private Button loginButton;
    private EditText usernameText, passwordText;
    private TextView createAccountLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        bindViews();
        setListeners();
    }

    private void bindViews() {
        loginButton = (Button) findViewById(R.id.login_button);
        usernameText = (EditText) findViewById(R.id.username_input);
        passwordText = (EditText) findViewById(R.id.password_input);
        createAccountLink = (TextView) findViewById(R.id.create_account_link);
    }

    private void setListeners() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                String urlBase="http://" + getString(R.string.ip_address) + "/user/login?";
                StringBuilder string = new StringBuilder();
                string.append(urlBase);
                string.append("username=");
                string.append(username);
                string.append("&password=");
                string.append(password);
                new arkaPlan().execute(string.toString());
            }
        });
        createAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginPage.this, SignupPage.class);
                startActivity(intent);
            }
        });

    }

    class arkaPlan extends AsyncTask<String, String, String> {

        protected String doInBackground (String ...params){
            HttpURLConnection connection = null;
            BufferedReader br = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String satir;
                String dosya = "";
                while ((satir = br.readLine()) != null) {
                    Log.d("satir:", satir);
                    dosya += satir;
                }
                return dosya;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hata var";
        }
        protected void onPostExecute(String s){
            if (s.equals("true")) {
                FirebaseMessaging.getInstance().subscribeToTopic(username);
                Intent intent = new Intent(LoginPage.this, AccountPage.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", username);
                intent.putExtras(bundle);
                startActivity(intent);
            }else if(s.equals("false")){
                Toast.makeText(getApplicationContext(), "Wrong password. Please try again", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(), "Connection problem", Toast.LENGTH_LONG).show();
            }
        }
    }
}

