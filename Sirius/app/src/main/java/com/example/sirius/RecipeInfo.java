package com.example.sirius;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RecipeInfo extends AppCompatActivity {

    private Button timelineButton, deleteButton;
    private String foodName;
    private String createDate;
    private String username;
    private String foodId;
    private TextView infoTitle;
    private TextView infoBody;
    private Button shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_info_layout);
        Bundle b = getIntent().getExtras();
        username = ""; // or other values
        if(b != null) {
            username = b.getString("username");
            createDate = b.getString("createDate");
            foodName = b.getString("foodName");
        }
        String urlBase = "http://" + getString(R.string.ip_address) +  "/food/listFoodsOf/?";
        StringBuilder string = new StringBuilder();
        string.append(urlBase);
        string.append("username=");
        string.append(username);
        new Background().execute(string.toString());
        deleteButton = (Button)findViewById(R.id.recipe_remove_button);
        timelineButton = (Button) findViewById(R.id.recipe_timeline_button);
        shareButton = (Button) findViewById(R.id.share_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("**********************", "onClick: ");
                final String url = "http://" + getString(R.string.ip_address) + "/food/deleteFood?foodId=" + foodId;
                RequestQueue queue = Volley.newRequestQueue(RecipeInfo.this);
                StringRequest dr = new StringRequest(Request.Method.DELETE, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Toast.makeText(getApplicationContext(), "Recipe is removed",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(RecipeInfo.this, AccountPage.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("username", username); //Your id
                                intent.putExtras(bundle); //Put your id to your next Intent
                                startActivity(intent);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "Could not be removed",Toast.LENGTH_LONG).show();
                            }
                        }
                );
                queue.add(dr);
            }
        });
        timelineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecipeInfo.this, AccountPage.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", username); //Your id
                intent.putExtras(bundle); //Put your id to your next Intent
                startActivity(intent);
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecipeInfo.this, LinkPop.class);
                Bundle bundle = new Bundle();
                String link = "http://" + getString(R.string.ip_address) + "/foodDetails?foodId=" + foodId;
                bundle.putString("link", link); //Your id
                intent.putExtras(bundle); //Put your id to your next Intent
                startActivity(intent);
            }
        });
    }

    class Background extends AsyncTask<String, String, String> {

        protected String doInBackground (String ...params){
            HttpURLConnection connection = null;
            BufferedReader br = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String satir;
                String dosya = "";
                while ((satir = br.readLine()) != null) {
                    Log.d("satir:", satir);
                    dosya += satir;
                }
                return dosya;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hata var";
        }
        protected void onPostExecute(String s){
            infoTitle = (TextView) findViewById(R.id.info_recipe_name);
            infoBody = (TextView) findViewById(R.id.info_recipe);
            String foodNametmp;
            String createDatetmp;
            String ingredients = "";
            String foodSteps = "";
            String lastComment = "";
            String tag = "";
            JSONArray ingredients_list= null;
            JSONArray cookstep_list = null;
            JSONArray lastphoto_list = null;
            try {
                final JSONArray obj = new JSONArray(s);
                final int n = obj.length();
                for (int i = 0; i < n; ++i) {
                    final JSONObject user = obj.getJSONObject(i);
                    foodNametmp = user.getString("foodName");
                    createDatetmp = user.getString("createDate");
                    if (foodNametmp.equals(foodName) && createDatetmp.equals(createDate)){
                        ingredients = user.getString("ingredients");
                        foodSteps = user.getString("foodSteps");
                        lastComment = user.getString("lastComment");
                        tag = user.getString("tag");
                        foodId = user.getString("foodId");
                        ingredients_list = user.getJSONArray("ingredientPhotos");
                        cookstep_list = user.getJSONArray("foodStepPhotos");
                        lastphoto_list = user.getJSONArray("lastPhotos");
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            infoBody.setTransformationMethod(null);
            infoBody.append("INGREDIENTS:\n"
                    + ingredients + "\n");
            if (ingredients_list != null) {
                try {
                    for (int i = 0 ; i<ingredients_list.length(); i++) {
                        String x = ingredients_list.get(i).toString();
                        byte[] decodedString = Base64.decode(x, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        DisplayMetrics dm = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(dm);
                        int width = dm.widthPixels;
                        int height = dm.heightPixels;
                        Bitmap last_bitmap = Bitmap.createScaledBitmap(decodedByte, (int)(width*0.8),(int)(height*0.4), true);
                        //Drawable d = new BitmapDrawable(getResources(), decodedByte);
                        SpannableString ss = new SpannableString("  ");
                        ss.setSpan(new ImageSpan(last_bitmap, ImageSpan.ALIGN_BASELINE), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        infoBody.append(ss);
                        infoBody.append("\n");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            infoBody.append("COOKING STEPS:\n"
                    + foodSteps + "\n");

            if (cookstep_list != null) {
                try {
                    for (int i = 0 ; i<cookstep_list.length(); i++) {
                        String x = cookstep_list.get(i).toString();
                        byte[] decodedString = Base64.decode(x, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        DisplayMetrics dm = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(dm);
                        int width = dm.widthPixels;
                        int height = dm.heightPixels;
                        Bitmap last_bitmap = Bitmap.createScaledBitmap(decodedByte, (int)(width*0.8),(int)(height*0.4), true);
                        //Drawable d = new BitmapDrawable(getResources(), decodedByte);
                        SpannableString ss = new SpannableString("  ");
                        ss.setSpan(new ImageSpan(last_bitmap, ImageSpan.ALIGN_BASELINE), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        infoBody.append(ss);
                        infoBody.append("\n");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            infoBody.append("LAST COMMENT:\n"
                    + lastComment + "\n");

            if (lastphoto_list != null) {
                try {
                    for (int i = 0 ; i<lastphoto_list.length(); i++) {
                        String x = lastphoto_list.get(i).toString();
                        byte[] decodedString = Base64.decode(x, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        DisplayMetrics dm = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(dm);
                        int width = dm.widthPixels;
                        int height = dm.heightPixels;
                        Bitmap last_bitmap = Bitmap.createScaledBitmap(decodedByte, (int)(width*0.8),(int)(height*0.4), true);
                        //Drawable d = new BitmapDrawable(getResources(), decodedByte);
                        SpannableString ss = new SpannableString("  ");
                        ss.setSpan(new ImageSpan(last_bitmap, ImageSpan.ALIGN_BASELINE), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        infoBody.append(ss);
                        infoBody.append("\n");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            infoBody.append("Tag:" + tag);
            /*
            // +  "\nLast comment:\n"
                    + lastComment + "\nTag:" + tag;
            infoBody.setText(info);
            */
            infoTitle.setText(foodName);

        }
    }



}
