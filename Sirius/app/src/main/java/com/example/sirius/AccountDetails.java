package com.example.sirius;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class AccountDetails extends AppCompatActivity {

    private String username;
    private Button update;
    private int schedule = 0;
    private EditText day;
    private TextView username_text;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_details);
        Bundle b = getIntent().getExtras();
        username = ""; // or other values
        if (b != null)
            username = b.getString("username");
        username_text = (TextView) findViewById(R.id.account_details_username);
        update = (Button) findViewById(R.id.account_update_button);
        day = (EditText) findViewById(R.id.account_details_day);
        username_text.setText(username);
        String urlBase = "http://" + getString(R.string.ip_address) + "/user/getUser/?username=";
        StringBuilder string = new StringBuilder();
        string.append(urlBase);
        string.append(username);
        new Background().execute(string.toString());
        setListener();
    }

    public void setListener(){
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RequestQueue queue = Volley.newRequestQueue(AccountDetails.this);

                String scheduleday = day.getText().toString();
                final String url = "http://" + getString(R.string.ip_address) +
                        "/scheduledDateAndroid?scheduledDate=" +scheduleday + "&username=" + username;
                // Optional Parameters to pass as POST request
                JSONObject js = new JSONObject();
                try {
                    js.put("scheduledDate",scheduleday);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Make request for JSONObject
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Intent intent = new Intent(AccountDetails.this, AccountPage.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("username", username); //Your id
                                intent.putExtras(bundle); //Put your id to your next Intent
                                startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("**", error.toString());
                        Toast.makeText(getApplicationContext(), "Connection problem. Please check your connection", Toast.LENGTH_LONG).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }

                };
                    // Adding request to request queue
                queue.add(postRequest);

            }
        });
    }

    class Background extends AsyncTask<String, String, String> {

        protected String doInBackground (String ...params){
            HttpURLConnection connection = null;
            BufferedReader br = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String satir;
                String dosya = "";
                while ((satir = br.readLine()) != null) {
                    dosya += satir;
                }
                Log.d("****errorr",dosya);
                return dosya;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hata var";
        }

        protected void onPostExecute(String s){
            try {
                Log.d("****errorr",s);
                final JSONObject obj = new JSONObject(s);
                int x = obj.getInt("scheduledDate");
                Log.d("****errorr",":("+x);
                day.setHint("" + x);
                Log.d("****errorr",":(");
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

}
