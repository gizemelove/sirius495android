package com.example.sirius;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateRecipe  extends AppCompatActivity {

    private Button takePictureButton;
    private static final String IMAGE_DIRECTORY = "/sirius";
    private int GALLERY = 1, CAMERA = 2;
    Uri file;
    private String username;
    private Button createRecipe;
    private EditText recipe_name,ingredients, cooking_steps, last_comment, hashtags;
    private TextView photo_ingredients, photo_cooking, photo_last;
    private int last_photo = 0;
    private ArrayList<String> ing_photos = new ArrayList<>();
    private ArrayList<String> cook_photos = new ArrayList<>();
    private ArrayList<String> last_photos = new ArrayList<>();
    private String arr []= {"123","1234"};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_recipe_layout);
        Bundle b = getIntent().getExtras();
        username = ""; // or other values
        if(b != null)
            username = b.getString("username");
        bindViews();
        setListeners();
    }

    private void bindViews() {
        createRecipe = (Button) findViewById(R.id.add_recipe_button);
        recipe_name = (EditText) findViewById(R.id.recipe_name_input);
        ingredients = (EditText) findViewById(R.id.ingredients_input);
        cooking_steps = (EditText) findViewById(R.id.cooking_steps_input);
        last_comment = (EditText) findViewById(R.id.last_comment_input);
        hashtags = (EditText) findViewById(R.id.hashtags_input);
        photo_ingredients = (TextView) findViewById(R.id.photo_ingredients_input);
        photo_cooking = (TextView) findViewById(R.id.photo_cooking_input);
        photo_last = (TextView) findViewById(R.id.photo_last_input);

    }
    private void setListeners() {

        photo_ingredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                last_photo = 1;
                showPictureDialog();
            }
        });

        photo_cooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                last_photo = 2;
                showPictureDialog();
            }
        });

        photo_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                last_photo = 3;
                showPictureDialog();
            }
        });

        createRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String recipe_name_ex = recipe_name.getText().toString();
                String ingredients_ex = ingredients.getText().toString();
                String cooking_steps_ex = cooking_steps.getText().toString();
                String last_comment_ex = last_comment.getText().toString();
                String hashtags_ex = hashtags.getText().toString();
                Log.i("sfdf", "onClick: RABİAAAAAAA");
                String url = "http://" + getString(R.string.ip_address) + "/food/addFood2";
                // Optional Parameters to pass as POST request
                JSONObject js = new JSONObject();
                try {
                    js.put("foodName",recipe_name_ex);
                    js.put("tag",hashtags_ex);
                    js.put("ingredients",ingredients_ex);
                    js.put("foodSteps",cooking_steps_ex);
                    js.put("lastComment",last_comment_ex);
                    js.put("username",username);
                    String ings = "";
                    if (ing_photos.size()>1)
                        for (int i = 0; i< ing_photos.size()-1; i++)
                            ings += ing_photos.get(i) + "GroupSirius";
                    else if (ing_photos.size() == 1)
                        ings = ing_photos.get(0);
                    String steps = "";
                    if (cook_photos.size()>1)
                        for (int i = 0; i< cook_photos.size()-1; i++)
                            steps += cook_photos.get(i) + "GroupSirius";
                    else if (cook_photos.size() == 1)
                        steps = cook_photos.get(0);
                    String last = "";
                    if (last_photos.size()>1)
                        for (int i = 0; i< last_photos.size()-1; i++)
                            last += last_photos.get(i) + "GroupSirius";
                    else if (last_photos.size() == 1)
                        last = last_photos.get(0);
                    js.put("ingredientPhotos",ings);
                    js.put("foodStepPhotos",steps);
                    js.put("lastPhotos",last);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Make request for JSONObject
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                        Request.Method.POST, url, js,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("**", response.toString());
                                Intent intent = new Intent(CreateRecipe.this, AccountPage.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("username", username); //Your id
                                intent.putExtras(bundle); //Put your id to your next Intent
                                startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("**", error.toString());
                        Toast.makeText(getApplicationContext(), "Oops! Something went wrong.", Toast.LENGTH_LONG).show();
                    }
                }) {

                    /**
                     * Passing some request headers
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }

                };
                // Adding request to request queue
                Volley.newRequestQueue(CreateRecipe.this).add(jsonObjReq);


            }
        });

    }


    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(CreateRecipe.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(CreateRecipe.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            saveImage(thumbnail);
            Toast.makeText(CreateRecipe.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        byte[] imageBytes = bytes.toByteArray();
        String imageString = Base64.getEncoder().encodeToString(imageBytes);
        if (last_photo == 1)
            ing_photos.add(imageString);
        else if (last_photo == 2)
            cook_photos.add(imageString);
        else if (last_photo == 3)
            last_photos.add(imageString);

        Log.i("*********", "saveImage: " + imageString);

        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }


}

